<?php

namespace App\Service;

use phpDocumentor\Reflection\Types\This;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiCovidService
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    public function __construct(HttpClientInterface $httpClient){
        $this->client = $httpClient;
    }

    public function getFranceData(): array
    {
        return($this->getApi('FranceLiveGlobalData'));
    }

    public function getAllData(): array
    {
        return($this->getApi('AllLiveData'));
    }

    public function getDepartmentData(string $department): array
    {
        return($this->getApi('LiveDataByDepartement?Departement=' . $department));
    }

    public function getAllDataByDate($date): array
    {
        return $this->getApi('AllDataByDate?date=' . $date);
    }

    public function getApi(string $var) :array
    {
        $response = $this->client->request(
            'GET',
            'https://coronavirusapi-france.now.sh/' . $var
        );
        return($response->toArray());

    }

}