<?php

namespace App\Controller;

use App\Service\CallApiCovidService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var CallApiCovidService
     */
    private $callApiCovidService;

    public function __construct( CallApiCovidService $callApiCovidService){
        $this->callApiCovidService = $callApiCovidService;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        //dd($this->callApiCovidService->getAllData());
        return $this->render('home/index.html.twig', [
            'data' => $this->callApiCovidService->getFranceData(),
            'departments' => $this->callApiCovidService->getAllData(),
        ]);
    }
}
